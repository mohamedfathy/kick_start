<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <title>Kick Start Interactive</title>

    <link rel="stylesheet" href="css/vendor/bootstrap_v4.4.1.css">
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 400px;
        width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }

      /* The popup bubble styling. */
      .popup-bubble {
        /* Position the bubble centred-above its parent. */
        position: absolute;
        top: 0;
        left: 0;
        transform: translate(-50%, -100%);
        /* Style the bubble. */
        background-color: white;
        padding: 5px;
        border-radius: 5px;
        font-family: sans-serif;
        overflow-y: auto;
        max-height: 60px;
        box-shadow: 0px 2px 10px 1px rgba(0,0,0,0.5);
      }
      /* The parent of the bubble. A zero-height div at the top of the tip. */
      .popup-bubble-anchor {
        /* Position the div a fixed distance above the tip. */
        position: absolute;
        width: 100%;
        bottom: /* TIP_HEIGHT= */ 8px;
        left: 0;
      }
      /* This element draws the tip. */
      .popup-bubble-anchor::after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        /* Center the tip horizontally. */
        transform: translate(-50%, 0);
        /* The tip is a https://css-tricks.com/snippets/css/css-triangle/ */
        width: 0;
        height: 0;
        /* The tip is 8px high, and 12px wide. */
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-top: /* TIP_HEIGHT= */ 8px solid white;
      }
      /* JavaScript will position this div at the bottom of the popup tip. */
      .popup-container {
        cursor: auto;
        height: 0;
        position: absolute;
        /* The max width of the info window. */
        width: 200px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-12">
        <?php include 'functions.php'; ?>

          <h3>First Task:</h3>
          <?php 
            $ar = array(4,4, 3,2,3,2,5, 3, 15, 3, 9, 5, 7, 9, 7); 
            echo "Unique element is ". find_unique_element_one($ar);
            echo "<hr />";
            echo "Unique element is ". find_unique_element_two($ar);
          ?>
          <hr />
          <h3>Second Task:</h3>
          <?php 
            $one = 'aax'; $two = 'bby'; $three = 'ccz';
            echo "Combined string is ". combine_words($one, $two, $three);
          ?>
          <hr />
          <h3>Third Task:</h3>
          <?php $cities = get_cities(); ?>
          <!-- start map -->
          <div id="map" style="height=400px;"></div>
          <div id="my_container"></div>
          <!-- end map -->
          <br />
          <br />
        </div>
      </div>
    </div>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/vendor/jquery_v3.4.1.js"></script>
    <script src="js/vendor/popper_v1.6.0.js"></script>
    <script src="js/vendor/bootstrap_v4.4.1.js"></script>
    <script>

        var map, popup, Popup;
        /** Initializes the map and the custom popup. */
        function initMap() {
          map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 0, lng: 0},
            zoom: 2,
          });
          var my_cities = <?php echo json_encode($cities); ?>;
          

          Popup = createPopupClass();
            for (const city of my_cities){

              switch(city.sentiment) {
                case 'Negative':
                  var color = 'red';
                  break;
                case 'Positive':
                  var color = 'green';
                  break;
                default:
                  var color = 'yellow';
              }

              // create a div on the fly
              var city_div = document.createElement("DIV");
              // adding an id to div
              city_div.id = city.city;
              city_div.style.backgroundColor = color;
              // adding a content message
              city_div.innerHTML = city.message +" <br />" + city.update_time;
              var container = document.getElementById('my_container');
              // append div to container
              container.appendChild(city_div);

              popup = new Popup(
                new google.maps.LatLng(city.latitude, city.longtiude),
                document.getElementById(city.city));
                popup.setMap(map);
            }

        }

        /**
         * Returns the Popup class.
         *
         * Unfortunately, the Popup class can only be defined after
         * google.maps.OverlayView is defined, when the Maps API is loaded.
         * This function should be called by initMap.
         */
        function createPopupClass() {
          /**
           * A customized popup on the map.
           * @param {!google.maps.LatLng} position
           * @param {!Element} content The bubble div.
           * @constructor
           * @extends {google.maps.OverlayView}
           */
          function Popup(position, content) {
            this.position = position;

            content.classList.add('popup-bubble');

            // This zero-height div is positioned at the bottom of the bubble.
            var bubbleAnchor = document.createElement('div');
            bubbleAnchor.classList.add('popup-bubble-anchor');
            bubbleAnchor.appendChild(content);

            // This zero-height div is positioned at the bottom of the tip.
            this.containerDiv = document.createElement('div');
            this.containerDiv.classList.add('popup-container');
            this.containerDiv.appendChild(bubbleAnchor);

            // Optionally stop clicks, etc., from bubbling up to the map.
            google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.containerDiv);
          }
          // ES5 magic to extend google.maps.OverlayView.
          Popup.prototype = Object.create(google.maps.OverlayView.prototype);

          /** Called when the popup is added to the map. */
          Popup.prototype.onAdd = function() {
            this.getPanes().floatPane.appendChild(this.containerDiv);
          };

          /** Called when the popup is removed from the map. */
          Popup.prototype.onRemove = function() {
            if (this.containerDiv.parentElement) {
              this.containerDiv.parentElement.removeChild(this.containerDiv);
            }
          };

          /** Called each frame when the popup needs to draw itself. */
          Popup.prototype.draw = function() {
            var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);

            // Hide the popup when it is far out of view.
            var display =
                Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                'block' :
                'none';

            if (display === 'block') {
              this.containerDiv.style.left = divPosition.x + 'px';
              this.containerDiv.style.top = divPosition.y + 'px';
            }
            if (this.containerDiv.style.display !== display) {
              this.containerDiv.style.display = display;
            }
          };

          return Popup;
        }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1kz6gxC9-ZrMKUATK4C4YdtmqqxBEG4o&callback=initMap">
    </script>
  </body>
</html>