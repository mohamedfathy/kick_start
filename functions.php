<?php 
    /**
    * Takes an array and return unique item in array
    *
    * @param   array $arr 
    * @return  $unique_element or false
    */
    function find_unique_element_one ($arr = []) 
    {
        if(count($arr) == 0) {return false;}
        $elements = array_count_values($arr);

        foreach ($elements as $key => $value) {
            if ($value == 1) {
                return $key;
            }
        }
        return false;
    }

    /**
    * Takes an array and return unique item in array
    *
    * @param   array $arr 
    * @return  $unique_element or false
    */
    function find_unique_element_two ($arr = []) 
    {
        $arr_count = count($arr);
        if($arr_count == 0) {return false;}

        $item = $arr[0]; 
        for ($i=1; $i<$arr_count; $i++) {
            $item = $item ^ $arr[$i];
        }

        return $item; 
    }


    /**
    * Takes three inputs and combine all the letters by squence 
    * and smash together.
    *
    * @param string $str_one
    * @param string $str_two
    * @param string $str_three
    * @return  $smashed_string
    */
    function combine_words ($str_one = '', $str_two = '', $str_three = '') 
    {
        if($str_one === '' || $str_two === '' || $str_three === '' ) {return false;}

        // convert the strings to array
        $arr_one = str_split($str_one);
        $arr_two = str_split($str_two);
        $arr_three = str_split($str_three);

        // loop and concatnate all the strings
        $count = count($arr_one); 
        $combined_string = '';
        for($i=0; $i<$count; $i++) {
            $combined_string .=  $arr_one[$i] . $arr_two[$i] .$arr_three[$i];
        }

        // return the string.
        return $combined_string; 
    }


    /**
    * call the api url using curl 
    *
    * @param void
    * @return  $response
    */
    function get_cities () 
    {
        $url = 'https://spreadsheets.google.com/feeds/list/0Ai2EnLApq68edEVRNU0xdW9QX1BqQXhHRl9sWDNfQXc/od6/public/basic?alt=json';

        $ch = curl_init();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_HTTPGET, true );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );

        $result = curl_exec ( $ch );
        curl_close ( $ch );
        $response = json_decode($result, true);

        $entries = $response['feed']['entry'];
        
        $ids = [];
        $messages = [];
        $questions = [];
        $update_times = [];
        $cities = [
            ['id' => 1, 'longtiude' => 36.278336, 'latitude' => 33.510414, 'city' => 'Damascus'],
            ['id' => 2, 'longtiude' => 45.318161, 'latitude' => 2.046934, 'city' => 'Mogadishu'],
            ['id' => 3, 'longtiude' => 1.43296, 'latitude' => 38.90883, 'city' => 'Ibiza'],
            ['id' => 4, 'longtiude' => 31.233334, 'latitude' => 30.033333, 'city' => 'Egypt'],
            ['id' => 5, 'longtiude' => 30.8418, 'latitude' => 29.30995, 'city' => 'Tahrir'],
            ['id' => 6, 'longtiude' => 36.817223, 'latitude' => -1.286389, 'city' => 'Nairobi'],
            ['id' => 7, 'longtiude' => 85.300140, 'latitude' => 27.700769, 'city' => 'Kathmandu'],
            ['id' => 8, 'longtiude' => -3.688344, 'latitude' => 40.453053, 'city' => 'Bernabau'],
            ['id' => 9, 'longtiude' => 23.727539, 'latitude' => 37.983810, 'city' => 'Athens'],
            ['id' => 10, 'longtiude' => 28.979530, 'latitude' => 41.015137, 'city' => 'Istanbul'],
        ];

        foreach ($entries as $entry) {
            list($ids[], $messages[], $sentiments[]) = explode(", ", $entry['content']['$t'], 3);
            $update_times[] = $entry['updated']['$t'];
        }

        $elements = [];
        for($i=0; $i<count($messages); $i++) {
            $elements[] = [
                'id' => substr($ids[$i], 11),
                'message' => substr($messages[$i], 9),
                'sentiment' => substr($sentiments[$i], 11),
                'latitude' => $cities[$i]['latitude'],
                'longtiude' => $cities[$i]['longtiude'],
                'city' => $cities[$i]['city'],
                'update_time' => $update_times[$i],
            ];
        }

        return $elements;
    }





